#!/bin/bash

FILES=$(git diff --cached --name-only)
TOOLPATH="./.git/hooks/google-java-format.jar"
JAVA_FILES=""
for file in $FILES; do
  if [[ ${file: -5} == ".java" ]]; then
    echo "Auto-formatting Java file $file"
    JAVA_FILES+="$file "
  fi
done

if [[ "$JAVA_FILES" != "" ]]; then
  java -jar "$TOOLPATH" -r $JAVA_FILES
fi
