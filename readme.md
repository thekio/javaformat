# JavaFormat

Sets up a git pre-commit hook that autoformats any commited java files using the [Google Java Format](https://github.com/google/google-java-format).

## Usage

After cloning the repository, run the hook installer specifying the path of the git repository:

```
cd javaformat && \
chmod +x ./format-installer.sh && \
./format-installer.sh </path/to/git/repo>
```

This action will create three new files in your repo's `.git/hooks` directory:

* pre-commit: the pre-commit hook
* pre-commit-java-format.sh: the script that invokes the formatter
* google-java-format.jar: the formatter jar

When performing a commit, the hook will intercept any java files in the commit stream and auto-format them. Additionally, it will print a message to stdout for each file processed. Example:

```
Auto-formatting Java file src/main/path/to/my/package/MyClass.java
```


