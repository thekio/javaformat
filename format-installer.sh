#!/bin/bash

#########################################################################
#
# Generates a Git Pre-Commit Hook using Google's Java Format, Version 1.5
#
#########################################################################

HOOKS_DIR=".git/hooks"
HOOK_NAME="pre-commit"
HOOK_SCRIPT="pre-commit-java-format.sh"
GOOGLE_FORMATTER_URL="https://github.com/google/google-java-format/releases/download/google-java-format-1.5/google-java-format-1.5-all-deps.jar"

try_get_repo_path() {
  local __retvar__="$1"
  local val="$2"

  if [[ ! -d "$val" ]]; then
    echo "Path $val is not a directory!"
    exit 1
  fi

  local err="$(git -C "$val" rev-parse 2>&1)"
  if [[ "$err" != "" ]]; then
    echo "Path $val is not a git repository!"
    exit 1
  fi

  eval $__retvar__="'$val'"
}

check_args() {
  if [[ "$#" != 1 ]]; then
    echo "Usage: format-installer.sh </path/to/git/repo>"
    exit 1
  fi
}

main() {
  #check cli args
  check_args $@

  #validate and get repo path
  local path
  try_get_repo_path path $1
  local hooks_path="$path/$HOOKS_DIR"

  #dl google-java-format jar
  curl -L -o "$hooks_path/google-java-format.jar" "$GOOGLE_FORMATTER_URL"

  #copy over the format.sh
  cp format.sh "$hooks_path/$HOOK_SCRIPT" && chmod +x "$hooks_path/$HOOK_SCRIPT"

  #create pre-commit hook script
  local precommit_path="$hooks_path/$HOOK_NAME"
  if [[ -f "$precommit_path" ]]; then
    rm "$precommit_path"
  fi
  touch "$precommit_path" && \
    chmod +x "$precommit_path" && \
    echo "./$HOOKS_DIR/$HOOK_SCRIPT" > "$precommit_path"
}

main $@
